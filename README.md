# Project 1

## Name
Dynamic website

## Description
This is a dynamic website coded with html, css, and js.

## Usage
To use this website, fill in all the required fields for the project that you want to create. Then, when every field is approved, press the add button to add the object to your dynamically generated table.

Buttons:
Write (save) local - will save the projects to your local files.
Append local - will append the unsaved projects to your local files.
Clear local - will clear the saved projects from local files.
Load (read) local - will read from your local saved files and render them in a table.



## Authors and acknowledgment
Artem Babin, 193365
Oscar Palencia, 2036561