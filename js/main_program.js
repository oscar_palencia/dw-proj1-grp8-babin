document.addEventListener("DOMContentLoaded", init);
//global variables
let all_projects_arr = [];
let patterns = {
  "proj-id": /.{2,50}/,
  "owner": /.{3,100}/,
  "title": /.{3,100}/,
  "category": /(pract|theory|fund_res|emp)/,
  "hours": /([0-9]|[1-9][0-9]|[1-9][0-9][0-9])/,
  "rate": /([0-9]|[1-9][0-9]|[1-9][0-9][0-9])/,
  "status": /(planned|ongoing|completed)/,
  "description": /.{3,1000}/,
};

// Main
function init() {
  let btns = document.querySelectorAll("input[type='button']");
  //disable "add" btn
  enable_disable_Button(btns[0], true);

  //disable "save local" btn
  enable_disable_Button(btns[2], true);

  //set all fields to be required
  let form1Labels = document.querySelectorAll("section.form-elem > label");
  for (i = 0; i < form1Labels.length; i++) {
    set_elem_required(form1Labels[i]);
  }

  form1Labels[3].required =false;
  form1Labels[6].required =false;

  //validate fields before creating project
  document.querySelectorAll("section.form-elem > input").forEach(item => {
    item.addEventListener("blur", function() {
      validateElement(item, form1Labels);
    })
  });
  
  //create project when "add" button is clicked
  btns[0].addEventListener("click", createProjectObject);

  //save projects to storage when button is clicked
  //not done
  btns[2].addEventListener("click", function () {
    if (localStorage.getItem("all_projects_arr") !== undefined) {
      enable_disable_Button(btns[2], true);
    } else {
      enable_disable_Button(btns[2], false);
    }
  });

  //other buttons
  //btns[3].addEventListener("click", appendAllProjects2Storage);
  btns[4].addEventListener("click", clearAllProjectsFromStorage);
  btns[5].addEventListener("click", readAllProjectsFromStorage);

  
   //when user clicks on query textbox, default text disappears
  //let enterQuery = document.querySelector("#enter-query");
  //enterQuery.addEventListener("onfocus", function (evt) {
    //evt.value = "";
  //});
}
