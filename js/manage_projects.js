/**
 * @author: Oscar Palencia
 */
function createProjectObject() {
  let project = {
    id: document.getElementById("proj-id").value,
    owner: document.getElementById("owner").value,
    title: document.getElementById("title").value,
    category: document.getElementById("category").value,
    status: document.getElementById("status").value,
    hours: document.getElementById("hours").value,
    rate: document.getElementById("rate").value,
    description: document.getElementById("description").value,
    edit: "",
    delete: "",
  };
  updateProjectsTable(project);
}

/**
 * @author: Oscar Palencia
 * @param {object} pr
 */
function updateProjectsTable(pr) {
  all_projects_arr.push(pr);
  let tableSection = document.querySelector("#table");
  createTableFromArrayObjects(all_projects_arr, tableSection);
}

/**
 * @author: Oscar Palencia
 * @param {object} project
 */
function deleteTableRow(project) {
  let res = confirm(`pls, confirm you want to delete project ${project}`);
  if (res == true) {
    index = all_projects_arr.findIndex((x) => x.id == project.id);
    let newArr = all_projects_arr.splice(index, 1);
    all_projects_arr = newArr;

    let tableSection = getElementById("table");
    createTableFromArrayObjects(all_projects_arr, tableSection);
  } else {
    alert("project not deleted");
  }
}

/**
 * @author: Artem Babin
 * @param {button} elem
 */
function editTableRow(elem) {
  let parent = elem.parentElement;
  let button = document.createElement("button");
  let gParent = parent.parentElement;

  button.id = "sbtn";
  button.style.backgroundImage = "url(../images/save.svg)";
  parent.innerHTML = "";
  parent.appendChild(button);

  console.log(gParent);

  for (const cell of gParent.cells) {
    console.log(cell);

    let t = cell.children;

    if (cell !== t.item(0)) {
      cell.innerHTML = `<input type='text' value='${cell.innerHTML}'>`;
    }
  }

  let sbtn = document.querySelector("#sbtn");
  let newProjects = [];
  sbtn.addEventListener("click", function () {
    createTableFromArrayObjects(newProjects, table);
  });
}

/**
 * @author: Oscar Palencia
 */
function saveAllProjects2Storage() {
  //todo: disable button if table (array) is empty
  //should do this in main_program (disable button and whenever button is clicked,
  //check if table is not empty to enable)
  localStorage.setItem("all_projects_arr", JSON.stringify(all_projects_arr));
  let statusSection = document.querySelector("#status");
  let status = document.createElement("p");
  let statusMessage = "all projects have been saved to local storage...";
  status.appendChild(statusMessage);
  statusSection.appendChild(status);
}

/**
 * @author: Oscar Palencia
 * @param {object} newData
 */
function appendAllProjects2Storage(newData) {
  let oldStorage = localStorage.getItem("all_projects_arr");
  if (oldStorage === null) oldStorage = "";
  localStorage.setItem("all_projects_arr", oldStorage + newData);
}

/**
 * @author: Oscar Palencia
 */
function clearAllProjectsFromStorage() {
  let res = confirm("pls, confirm you want to delete ALL projects");
  if (res == true) {
    localStorage.removeItem("all_projects_arr");
    let statusSection = document.querySelector("#status");
    let status = document.createElement("p");
    let statusMessage = "all projects have been removed from local storage ...";
    status.appendChild(statusMessage);
    statusSection.appendChild(status);
  } else {
    alert("projects not deleted");
  }
}

/**
 * @author: Oscar Palencia
 */
function readAllProjectsFromStorage() {
  let studentStr = localStorage.getItem("all_projects_arr");
  let storage_projects_arr = JSON.parse(studentStr);

  let tableSection = getElementById("table");
  createTableFromArrayObjects(storage_projects_arr, tableSection);
}

/**
 * @author: Oscar Palencia
 */
function filterProjects() {
  let filterProjArr = [];
  let count = 0;
  all_projects_arr.forEach(function (elem) {
    for (query of elem) {
      if (query.value == elem.value) {
        filterProjArr.push(elem);
        count++;
        break;
      }
    }
  });
  let statusSection = document.querySelector("#status");
  let status = document.createElement("p");
  let statusMessage = `There is/are ${count} projects for your query`;
  status.appendChild(statusMessage);
  statusSection.appendChild(status);

  let tableSection = getElementById("table");
  createTableFromArrayObjects(filterProjArr, tableSection);
}
