/**
 * @author: Artem Babin
 * @param {object} btn
 * @param {boolean} condition
 */

function enable_disable_Button(btn, condition) {
  btn.disabled = condition;
}

/**
 * @author: Artem Babin,
 * @param {ojbect} elem
 */
function set_elem_required(elem) {
  elem.setAttribute("required", "true");
  elem.style.fontWeight = "bold";
}

/**
 * @author: Artem Babin
 * @param {object} tag_object
 * @param {object} parent
 */
function createTagElement(tag_object, parent) {
  let tagElement = tag_object.tagName;
  document.createElement(tagElement);

  let parentElement = document.querySelector(parent);
  parentElement.appendChild(tagElement);
}

/**
 * @author: Artem Babin
 * @param {object} elem_id
 * @param {String} feedback_text
 * @param {boolean}} error_success
 */
function setElementFeedback(elem_id, feedback_text, error_success) {
  let elem = document.getElementById(elem_id);
  let parent = elem.parentElement;
  //let oldMessage = document.querySelector("parent > text");
  //let oldImg = document.querySelector("parent > img");

  // Remove existing image
  if (document.contains(document.getElementById("img"))) {
    document.getElementById("img").remove();
  }

  if (error_success == false) {
    let textnode = document.createTextNode(feedback_text);
    let message = document.createElement("text");
    message.appendChild(textnode);
    //parent.removeChild(oldMessage);
    parent.appendChild(message);

    let img = document.createElement("img");
    img.class = "img";
    img.src = "../images/wrong.svg";
    //parent.removeChild(oldImg);
    parent.appendChild(img);
  } else {
    let img = document.createElement("img");
    img.class = "img";
    img.src = "../images/valid.svg";
    //parent.removeChild(oldImg);
    parent.appendChild(img);
  }
}

/**
 * @author: Oscar Palencia, Artem Babin
 * @param {object} input
 * @param {object} formElems
 */
function validateElement(input, formElems) {
  if (patterns[input.id].test(input.value) == false) {
    setElementFeedback(input.id, "", false);
    input.required = false;
  } else {
    let feedback_text = `Wrong format for ${input.id}`;
    setElementFeedback(input.id, feedback_text, true);
    input.required = true;
  }
  let disable_add_btn = true;
  for (elem in formElems) {
    if (formElems[elem].required == true) {
      disable_add_btn = true;
      break;
    }
    addBtn = document.querySelector("#add");
    enable_disable_Button(addBtn, disable_add_btn);
  }
}

/**
 * @author: Artem Babin
 * @param {array} projects
 * @param {object} parent
 */
function createTableFromArrayObjects(projects, parent) {
  // Removes existing table
  if (document.contains(document.querySelector("table"))) {
    document.querySelector("table").remove();
  }

  // Variables
  let table = document.createElement("table");
  let tBody = document.createElement("tbody");
  let tr = document.createElement("tr");
  let objKey = Object.keys(projects[0]);
  parent.appendChild(table);

  // Header table
  for (let i = 0; i < objKey.length; i++) {
    let th = document.createElement("th");
    let text = document.createTextNode(objKey[i]);
    th.appendChild(text);
    tr.appendChild(th);
  }
  table.appendChild(tr);

  // Creating table body rows and columns
  for (let i = 0; i < projects.length; i++) {
    let tr = document.createElement("tr");
    let objValue = Object.values(projects[i]);

    for (let j = 0; j < objKey.length; j++) {
      let td = document.createElement("td");
      if (objKey[j] == "edit") {
        let button = document.createElement("button");
        button.id = "ebtn";
        button.className = "tablebtn";
        button.style.backgroundImage = "url(../images/edit.svg)";

        td.appendChild(button);
        tr.appendChild(td);
      } else if (objKey[j] == "delete") {
        let button = document.createElement("button");
        button.id = "dbtn";
        button.className = "tablebtn";
        button.style.backgroundImage = "url(../images/delete.svg)";

        td.appendChild(button);
        tr.appendChild(td);
      } else {
        let text = document.createTextNode(objValue[j]);
        td.appendChild(text);
        tr.appendChild(td);
      }
    }
    tBody.appendChild(tr);
  }
  table.appendChild(tBody);
  addEditDeleteButtons();
}

/**
 * @author: Artem Babin
 */
function addEditDeleteButtons() {
  let ebtn = document.querySelector("#ebtn");
  let dbtn = document.querySelector("#dbtn");

  ebtn.addEventListener("click", function () {
    editTableRow(ebtn);
  });

  dbtn.addEventListener("click", deleteTableRow);
}
